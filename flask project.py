from flask import Flask, render_template, request, redirect, url_for, session, flash


app = Flask(__name__)

#secret key used for encryption
app.secret_key = "my secrey key"

@app.route('/')
@app.route('/home')
def home():
    return render_template('index.html')

@app.route('/login', methods=['GET','POST'])
def login():
    error = None
    if request.method=='POST':
        if request.form['username']!='admin' or request.form['password']!='admin':
            error = 'Invalid credentials'
        else:
            session['logged_in'] = True
            flash('you were just logged in')
            return redirect(url_for('home'))

    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('you were just logged out')
    return redirect(url_for('home'))

''' you can specify the methods in the routing method'''
@app.route("/profile/<userid>", methods=['GET'])
#if you want to fetch the querystring variables in the routing then use <variablename>
def profile(userid):
    #render template is basically renders the html with the required attributes
    return render_template('profile.html', name=userid)

''' you can use multiple url and pass the arguments as default values'''
@app.route('/user')
@app.route("/user/<userid>", methods=['GET'])
def userprofile(userid=None):
    #render template is basically renders the html with the required attributes
    return render_template('user.html', userid=userid)

''' passing a list to render template example'''
@app.route('/shopping')
def shopping():
    food = ['Shirts', 'Trousers']
    return render_template('shopping.html', food=food)

if __name__ == '__main__':
    app.run(debug=True)
